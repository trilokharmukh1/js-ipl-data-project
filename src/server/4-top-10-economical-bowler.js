// this function find out top 10 economical bowler based on year result will dump into JSON file

const csv = require('csvtojson');
const fs = require('fs');
const path = require('path');

function top10EconomicalBowler(year) {
    csv()
        .fromFile(path.join(__dirname, './../data/matches.csv'))
        .then((allMatches) => {
            csv()
                .fromFile(path.join(__dirname, './../data/deliveries.csv'))
                .then((allDeliveries) => {

                    let matchId = allMatches.filter((match) => {
                        return match.season == year;
                    })
                        .map((match) => {
                            return match.id;
                        });

                    // getting all bowler details with runs and balls
                    let economyOfAllBowler = allDeliveries.reduce((bolwerDetais, delevery) => {
                        if (matchId.includes(delevery.match_id)) {

                            let bowler = delevery.bowler;
                            let batsmanRuns = parseInt(delevery.batsman_runs);
                            let extraRuns = parseInt(delevery.extra_runs);

                            if (bolwerDetais[bowler]) {

                                if (delevery.wide_runs > 0 || delevery.noball_runs > 0) {
                                    bolwerDetais[bowler].runs += batsmanRuns + extraRuns;

                                } else if ((delevery.wide_runs == 0 && delevery.noball_runs == 0) && (delevery.legbye_runs > 0 || delevery.noball_runs > 0)) {
                                    bolwerDetais[bowler].ball++;

                                } else {
                                    bolwerDetais[bowler].ball++;
                                    bolwerDetais[bowler].runs += batsmanRuns + extraRuns;
                                }

                            } else {
                                bolwerDetais[bowler] = { ball: 1, runs: batsmanRuns + extraRuns }

                            }
                        }
                        return bolwerDetais;
                    }, {});

                    //calculte the bowler's economy and returns the top 10 ewconomical bowlers 
                    economyOfAllBowler = Object.entries(economyOfAllBowler)
                        .map((bowler) => {
                            let economy = ((bowler[1].runs / bowler[1].ball) * 6).toFixed(2);
                            bowler[1]["economy"] = economy;
                            return bowler;
                        })
                        .sort((bowlerA, bowlerB) => {
                            return bowlerA[1].economy - bowlerB[1].economy;
                        }).splice(0, 10)
                        .reduce((acc, bowlerDetail) => {
                            acc["top 10 boller"] = { ...acc["top 10 boller"], [bowlerDetail[0]]: bowlerDetail[1] }
                            return acc;
                        }, {});

                    // dump result into JSON file
                    fs.writeFile(
                        path.join(__dirname, './../public/output/4-top-10-economical-bowler.json'),
                        JSON.stringify(economyOfAllBowler),
                        (err) => {
                            if (err) {
                                console.error("error: ", err);
                            }
                        });

                });
        });

}

top10EconomicalBowler(2015);