// this module find out how many matches won per team per year and result will dump into JSON file

const csv = require('csvtojson');
const fs = require('fs');
const path = require('path');

function matchesWonPerTeamPerYear() {
    csv()
        .fromFile(path.join(__dirname, './../data/matches.csv'))
        .then((allMatches) => {

            let result = allMatches.reduce((matchesWonPerTeam, match) => {
                if (matchesWonPerTeam[match.season]) {
                    if (matchesWonPerTeam[match.season][match.winner]) {
                        matchesWonPerTeam[match.season][match.winner]++;
                    } else {
                        if (match.winner != "") {  // if match has no result then they will not be added
                            matchesWonPerTeam[match.season][match.winner] = 1;
                        }
                    }

                } else {
                    if (match.winner != "") {
                        matchesWonPerTeam[match.season] = { [match.winner]: 1 };
                    }
                }

                return matchesWonPerTeam
            }, {});

            // dump result into JSON file
            fs.writeFile(
                path.join(__dirname, './../public/output/2-matches-won-per-team-per-year.json'),
                JSON.stringify(result), (err) => {
                    if (err) {
                        console.log("error:", err);
                    }
                });
        });
}

matchesWonPerTeamPerYear();