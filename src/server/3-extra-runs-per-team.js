// this module find out extra runs per team and result will dump into JSON file

const csv = require('csvtojson')
const fs = require('fs')
const path = require('path');

function extraRunsPerTeam(year) {
    csv()
        .fromFile(path.join(__dirname, './../data/matches.csv'))
        .then((allMatches) => {
            csv()
                .fromFile(path.join(__dirname, './../data/deliveries.csv'))
                .then((allDeliveries) => {

                    let matchId = allMatches.filter((match) => {
                        return match.season == year;
                    })
                        .map((year) => {
                            return year.id;
                        });

                    let result = allDeliveries.reduce((extraRuns, delivery) => {
                        if (matchId.includes(delivery.match_id)) {
                            extraRuns[delivery.bowling_team]
                                ? extraRuns[delivery.bowling_team] += Number(delivery.extra_runs)
                                : extraRuns[delivery.bowling_team] = Number(delivery.extra_runs)
                        }
                        return extraRuns;
                    }, {});

                    result = { [year + "'s extra runs per team"]: result };

                    // dump result into JSON file
                    fs.writeFile(
                        path.join(__dirname, './../public/output/3-extra-runs-per-team.json'),
                        JSON.stringify(result), (err) => {
                            if (err) {
                                console.log("error:", err);
                            }
                        })
                })

        });
}

extraRunsPerTeam(2016);