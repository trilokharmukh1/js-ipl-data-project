// this function find out the bowler with the best economy in super overs and result will dump into JSON file

const csv = require('csvtojson');
const fs = require('fs');
const path = require('path');

function bestEconomyInSuperOver() {
    csv()
        .fromFile(path.join(__dirname, './../data/deliveries.csv'))
        .then((allDeliveries) => {

            let bowlerInSuperOver = allDeliveries.reduce((bowlerDetails, delivery) => {

                if (delivery.is_super_over == 1) {
                    let batsmanRuns = parseInt(delivery.batsman_runs);
                    let extraRuns = parseInt(delivery.extra_runs);

                    if (bowlerDetails[delivery.bowler]) {

                        if (delivery.wide_runs > 0 || delivery.noball_runs > 0) {  // in this case only run will be added
                            bowlerDetails[delivery.bowler].runs += batsmanRuns + extraRuns;
                      
                        } else if ((delivery.wide_runs == 0 && delivery.noball_runs == 0) && (delivery.legbye_runs > 0 || delivery.bye_runs > 0)) { //in this case only ball will be counted
                            bowlerDetails[delivery.bowler].ball++;

                        } else {  // ball and runs will be added
                            bowlerDetails[delivery.bowler].ball++;
                            bowlerDetails[delivery.bowler].runs += batsmanRuns + extraRuns;
                        }

                    } else {
                        if (delivery.wide_runs > 0 || delivery.noball_runs > 0) {  // in this case only run will be added
                            bowlerDetails[delivery.bowler] = { ball: 0, runs: batsmanRuns + extraRuns };

                            //in this case only ball will be counted
                        } else if ((delivery.wide_runs == 0 && delivery.noball_runs == 0) && (delivery.legbye_runs > 0 || delivery.bye_runs > 0)) {
                            bowlerDetails[delivery.bowler] = { ball: 1, runs: 0 }

                        } else {  // ball and runs will be added
                            bowlerDetails[delivery.bowler] = { ball: 1, runs: batsmanRuns + extraRuns }
                        }
                    }
                }

                return bowlerDetails;
            }, {});


            let result = Object.entries(bowlerInSuperOver)
                .map((bowler) => {
                    let economy = ((bowler[1].runs / bowler[1].ball) * 6).toFixed(2);
                    bowler[1]["economy"] = economy;
                    return bowler;
                })
                .sort((bowlerA, bowlerB) => {
                    return bowlerA[1].economy - bowlerB[1].economy;
                })[0];

            // result will be store into JSON file
            fs.writeFile(
                path.join(__dirname, './../public/output/9-bowler-with-best-economy-in-super-overs.json'),
                JSON.stringify(result),
                (err) => {
                    if (err) {
                        console.log("error: ", err);
                    }
                });
        });
}

bestEconomyInSuperOver()
