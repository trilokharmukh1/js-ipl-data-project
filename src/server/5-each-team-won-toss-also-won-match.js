// this module find out team who won the toss and also won the match and result will dump into JSON file

const csv = require('csvtojson')
const fs = require('fs')
const path = require('path');

function wonTossAndWonMatch() {
  csv()
    .fromFile(path.join(__dirname, './../data/matches.csv'))
    .then((allMatches) => {

      let result = allMatches.reduce((matchWon, match) => {
        if (match.toss_winner == match.winner) {
          if (matchWon[match.toss_winner]) {
            matchWon[match.toss_winner]++;
          } else {
            matchWon[match.toss_winner] = 1;
          }
        }
        return matchWon;
      }, {});

      // store result into JSON file
      fs.writeFile(
        path.join(__dirname, './../public/output/5-each-team-won-toss-also-won-match.json'),
        JSON.stringify(result), (err) => {
          if (err) {
            console.log("error:", err);
          }
        });

    });
}

wonTossAndWonMatch();