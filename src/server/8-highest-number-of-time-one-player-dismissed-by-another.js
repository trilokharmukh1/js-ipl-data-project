// this function find out highest number of times one player has been dismissed by another player and result will dump into JSON file

const csv = require('csvtojson')
const fs = require('fs');
const path = require('path');

function onePlyerDissmissedByAnother(batsman) {
    csv()
        .fromFile(path.join(__dirname, './../data/deliveries.csv'))
        .then((allDeliveries) => {

            let playerDismissedBy = allDeliveries.reduce((player, delivery) => {

                if ((delivery.batsman == batsman && delivery.player_dismissed !== "")) {
                    if (delivery.bowler in player) {
                        if (delivery.dismissal_kind != "run out") {     // runout not counted bowler's wicket
                            player[delivery.bowler] += 1;
                        }
                    } else {
                        if (delivery.dismissal_kind != "run out") {
                            player[delivery.bowler] = 1;
                        }
                    }

                    //if batsman dissmissed by run out, in this case wicket taked by fielder
                    if (delivery.dismissal_kind == "run out") {
                        if (delivery.fielder in player) {
                            player[delivery.fielder] += 1;
                        } else {
                            player[delivery.fielder] = 1;
                        }
                    }
                }

                return player;

            }, {});


            playerDismissedBy = Object.entries(playerDismissedBy)
                .sort((player1, player2) => {
                    return player2[1] - player1[1];
                });

            let max = playerDismissedBy[0][1];
            playerDismissedBy = playerDismissedBy.filter((bowler) => {
                return max == bowler[1];
            })
                .reduce((acc, curr) => {
                    acc[curr[0]] = curr[1]
                    return acc;
                }, {});

            let result = {};
            result[batsman + " dismissed most nummber of times by"] = playerDismissedBy;

            // store result into JSON file
            fs.writeFile(
                path.join(__dirname, './../public/output/8-highest-number-of-time-one-player-dismissed-by-another.json'),
                JSON.stringify(result),
                (err) => {
                    if (err) {
                        throw new Error(err);
                    }
                });
        })

}

onePlyerDissmissedByAnother('DA Warner');
