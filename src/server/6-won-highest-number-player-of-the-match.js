// this module find out player who has won the highest number of Player of the Match awards for each season

const csv = require('csvtojson');
const fs = require('fs');
const path = require('path');

function wonHighesNumberOfPlayerOfMatch() {

    csv()
        .fromFile(path.join(__dirname, './../data/matches.csv'))
        .then((allMatches) => {

            let playerOfMatch = allMatches.reduce((players, match) => {
                if (players[match.season]) {
                    if (players[match.season][match.player_of_match]) {
                        players[match.season][match.player_of_match]++;
                    } else {
                        players[match.season][match.player_of_match] = 1;
                    }
                } else {
                    players[match.season] = { [match.player_of_match]: 1 };
                }
                return players;
            }, {});

            let result = Object.entries(playerOfMatch)
                .map(([year, player]) => {
                    return [
                        year,
                        Object.entries(player)
                            .sort((first, second) => {
                                return second[1] - first[1];
                            })[0]
                    ];
                })
                .reduce((acc, player) => {
                    acc[player[0]] = { [player[1][0]]: player[1][1] }
                    return acc;
                }, {})

            // store result into JSON file
            fs.writeFile(
                path.join(__dirname, './../public/output/6-won-highest-number-player-of-the-match.json'),
                JSON.stringify(result),
                (err) => {
                    if (err) {
                        throw new Error(err);
                    }
                });

        });

}

wonHighesNumberOfPlayerOfMatch();