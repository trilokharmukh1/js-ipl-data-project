// this function find out strick rate of batsman and result will be dump into JSON file

const csv = require('csvtojson');
const fs = require('fs');
const path = require('path');

function strikeRateOfBatsmanEachSeason(batsman) {
    csv()
        .fromFile(path.join(__dirname, './../data/matches.csv'))
        .then((allMatches) => {
            csv()
                .fromFile(path.join(__dirname, './../data/deliveries.csv'))
                .then((allDeliveries) => {

                    let matchId = allMatches.map((match) => {
                        return match.id;
                    });

                    // return batsman details which cobtain season, balls and runs
                    let batsmanRunsEachSeason = allDeliveries.reduce((strikeRateOfBatsman, delivery) => {
                        if (delivery.batsman == batsman) {
                            let idIndex = matchId.indexOf(delivery.match_id);
                            let season = allMatches[idIndex].season;

                            if (strikeRateOfBatsman[season]) {
                                if (delivery.wide_runs == 0) {
                                    strikeRateOfBatsman[season]["balls"] += 1;
                                }
                                strikeRateOfBatsman[season]["runs"] += parseInt(delivery.batsman_runs);
                            } else {
                                strikeRateOfBatsman[season] = { "balls": 1, "runs": parseInt(delivery.batsman_runs) };
                            }
                        }

                        return strikeRateOfBatsman;
                    }, {});

                    // calculate strike rate
                    let result = Object.entries(batsmanRunsEachSeason).reduce((acc, batsmanData) => {
                        let strikeRate = ((parseInt(batsmanData[1].runs) / parseInt(batsmanData[1].balls)) * 100).toFixed(2);
                        batsmanData[1]["strick_rate"] = strikeRate;
                        acc[batsman + "'s strikeRate"] = { ...acc[batsman + "'s strikeRate"], [batsmanData[0]]: batsmanData[1] };
                        return acc;
                    }, {});

                    //  result will be store into JSON file
                    fs.writeFile(
                        path.join(__dirname, './../public/output/7-strike-rate-of-batsman-each-season.json'),
                        JSON.stringify(result),
                        (err) => {
                            if (err) {
                                console.log("Error: ", err);
                            }
                        });
                });
        });


}

strikeRateOfBatsmanEachSeason("DA Warner");