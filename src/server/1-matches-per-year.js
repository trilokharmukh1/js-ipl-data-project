const fs = require('fs');
const csv = require('csvtojson');
const path = require('path'); 

function matchesPerYear() {
  csv()
    .fromFile(path.join(__dirname, './../data/matches.csv'))
    .then((allMatches) => {

      let result = allMatches.reduce((matchesPerYear, match) => {
        matchesPerYear[match.season]
          ? matchesPerYear[match.season]++
          : matchesPerYear[match.season] = 1;
        return matchesPerYear;
      }, {});

      // dump object to JSON file
      fs.writeFile(
        path.join(__dirname, './../public/output/1-matches-per-year.json'),
        JSON.stringify(result),
        (err) => {
          if (err) {
            console.log("error", err);
          }
        });

    });
}

matchesPerYear();